# Project Gutemberg API

## Prerequisites
* Node.js
* RethinkDB

## Install

Clone the repository and install dependencies:
```
git clone git@bitbucket.org:alfredormz/gutenberg.git
cd gutenberg
npm install
```

Then edit the `config/default.yml.sample` and save it as `config/default.yml`

Finally

```
grunt 
```
